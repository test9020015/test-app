import getConfig from 'next/config'

interface IRuntimeConfig {
  HOST: string
}

interface IServerRuntimeConfig {
  HOST_INNER?: string
}

function getRuntimeConfig(): IRuntimeConfig {
  const { publicRuntimeConfig } = getConfig()
  return publicRuntimeConfig
}

function getServerRuntimeConfig(): IServerRuntimeConfig {
  const { serverRuntimeConfig } = getConfig()
  return serverRuntimeConfig
}

export const runtimeConfig = getRuntimeConfig()

export const serverRuntimeConfig = getServerRuntimeConfig()
