import styles from './index.module.scss'
import Button from '@/components/ui/Button'
import { useAppContext } from '@/context/state'
import ModalLayout from '../ModalLayout'


interface Props {
  
}

export default function OrderSendedModal(props: Props) {

  const appContext = useAppContext()

  return (
    <ModalLayout onRequestClose={appContext.hideModal} title='Заказ успешно отправлен!'>
      <Button onClick={appContext.hideModal} className={styles.btn}>
        OK
      </Button>
    </ModalLayout >
  )
}
