import { FieldConfig, FieldHookConfig, useField, } from 'formik'
import styles from './index.module.scss'
import { useEffect } from 'react'
import { IField } from 'types/types'
import { useMask } from '@react-input/mask'
import { useOrderContext } from '@/context/order'

interface Props extends IField<string> {
  className?: string
}

export default function PhoneField(props: Props & FieldConfig) {
  const [field, meta, helpers] = useField(props as FieldHookConfig<any>)
  const showError = !!meta.error

  const orderContext = useOrderContext()

  const mask = '+7 (___) ___-__-__'

  const handleChange = (value: string) => {
    helpers.setValue(value)
    orderContext.changePhoneNumber(value)
  }

  useEffect(() => {
    if (!field.value) {
      helpers.setValue(mask)
      orderContext.changePhoneNumber(mask)
    }
  }, [field.value])

  const inputRef = useMask({ mask: mask, replacement: { _: /\d/ } })

  return (
    <input className={styles.root}
      name={field.name} ref={inputRef}
      value={field.value}
      onChange={(e) => handleChange(e.target.value)} />
  )
}
