import { IProduct } from '@/data/interfaces/IProduct'
import styles from './index.module.scss'
import { useState } from 'react'
import ProductCard from './ProductCard'
import InfiniteScroll from 'react-infinite-scroll-component'
import Spinner from '../ui/Spinner'
import ProductsRepository from '@/data/repositories/ProductsRepository'

interface Props {
  products: IProduct[]
  totalItems: number
  whichPage: number
}

export default function Products({ products, totalItems, whichPage }: Props) {

  const [items, setItems] = useState<IProduct[]>(products)
  const [page, setPage] = useState<number>(whichPage)
  const [total, setTotal] = useState<number>(totalItems)
  const [loading, setLoading] = useState<boolean>(false)
  const limit = 6

  items.map((i, index) =>
    <ProductCard className={styles.card} card={i} key={i.id} />
  )

  const handleScrollNext = async () => {
    setPage(page + 1)
    setLoading(true)
    await ProductsRepository.fetch(page + 1, limit).then(data => {
      if (data) {
        setItems(items => [...items, ...data.products])
      }
    })
    setLoading(false)
  }

  return (
    <div className={styles.root}>
      {loading && total === 0 && <Spinner size={200} />}
      {total > 0 && <InfiniteScroll
        dataLength={items.length} //This is important field to render the next data
        next={handleScrollNext}
        hasMore={total > items.length}
        loader={loading ? <Spinner size={200} /> : null}
        className={styles.wrapper}
        scrollableTarget='scrollableDiv'>
        {items.map((i, index) =>
          <ProductCard className={styles.card} card={i} key={i.id} />
        )}
      </InfiniteScroll>}
    </div >
  )
}
