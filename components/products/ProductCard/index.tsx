import Image from 'next/image'
import styles from './index.module.scss'
import { IProduct } from '@/data/interfaces/IProduct'
import classNames from 'classnames'
import Button from '@/components/ui/Button'
import { useOrderContext } from '@/context/order'
import { ICartItem } from '@/data/interfaces/ICartItem'
import QuantityInput from '@/components/ui/QuantityInput'

interface Props {
  card: IProduct
  className?: string
}

export default function ProductCard({ card, className }: Props) {

  const orderContext = useOrderContext()

  const handleRemove = (item: ICartItem) => {
    if (item.quantity === 1) {
      orderContext.removeItemFromCart(card.id)
    }
    else {
      orderContext.updateItemQuantity(card.id, --item.quantity)
    }
  }

  const item = orderContext.order.cart.find(i => i.id === card.id)

  const handleChange = (itemId: number, quantity: number) => {
    if (quantity === 0) {
      orderContext.removeItemFromCart(card.id)
    }
    else {
      orderContext.updateItemQuantity(itemId, quantity)
    }
  }

  return (
    <div className={classNames(styles.root, className)}>
      <div className={styles.top}>
        <div className={styles.image}>
          <Image className={styles.image} src={card.image_url} alt='' width={281} height={366} />
        </div>
        <div className={styles.title}>
          {card.title}
        </div>
        <div className={styles.desc}>
          {card.description}
        </div>
      </div>
      <div className={styles.bottom}>
        <div className={styles.price}>
          цена: {card.price}
        </div>
        {!item ?
          <Button
            onClick={() => orderContext.addNewItemToCart({ id: card.id, quantity: 1, title: card.title, price: card.price })}
            className={styles.btn}>Купить</Button>
          :
          <div className={styles.btns}>
            <Button className={styles.controls}
              onClick={() => handleRemove(item)}>-</Button>
            <QuantityInput value={item.quantity.toString()} onChange={(value) => handleChange(card.id, +value)} />
            <Button onClick={() => orderContext.updateItemQuantity(card.id, ++item.quantity)} className={styles.controls}>+</Button>
          </div>}
      </div>
    </div >
  )
}
