import { useState } from 'react'
import styles from './index.module.scss'
import { IReview } from '@/data/interfaces/IReview'
import ReviewCard from './ReviewCard'

interface Props {
  reviews: IReview[]
}

export default function Reviews({ reviews }: Props) {

  const [items, setItems] = useState<IReview[]>(reviews)

  return (
    <div className={styles.root}>
      {items.map((i, index) =>
        <ReviewCard content={i.text} key={i.id} />
      )}
    </div>
  )
}
