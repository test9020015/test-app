import styles from './index.module.scss'
import HtmlText from '@/components/ui/HtmlText'

interface Props {
  content: string
}

export default function ReviewCard({ content }: Props) {

  return (
    <div className={styles.root}>
      <HtmlText html={content} />
    </div>
  )
}
