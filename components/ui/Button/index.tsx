import styles from './index.module.scss'
import classNames from 'classnames'
import Link from 'next/link'
import Spinner from 'components/ui/Spinner'
import { ReactElement, RefObject } from 'react'
import { IButton } from '@/types/types'

export interface ButtonProps extends IButton {
  children?: React.ReactNode | null | string
  className?: string
  fluid?: boolean
  buttonRef?: RefObject<any>
  press?: boolean
  hover?: boolean
  stopPropagation?: boolean
  icon?: ReactElement
  reverse?: boolean
  circle?: boolean
}

export default function Button(props: ButtonProps) {

  const cn = classNames({
    [styles.root]: true,
    [styles.disabled]: props.disabled,
    [styles.fluid]: props.fluid,
    [styles.withIcon]: !!props.icon,
  }, props.className)

  if (props.href) {
    return (
      <Link href={props.href}
        className={classNames(cn)}
        target={props.isExternalHref ? '_blank' : ''}
        rel={props.isExternalHref ? 'noreferrer' : ''}
        onClick={(e) => props.onClick?.(e)}>
        <span className={classNames({
          [styles.text]: true,
          [styles.textHidden]: props.spinner,
        })}>  {props.icon}{props.children}</span>
      </Link>
    )
  }

  return (
    <button
      type={props.type}
      form={props.form}
      onClick={(e) => {
        if (props.stopPropagation) {
          e.stopPropagation()
        }
        if (props.onClick && !props.spinner && !props.disabled) {
          props.onClick(e)
        }
      }}
      disabled={props.disabled || props.spinner}
      className={classNames(cn)}
    >
      <span className={classNames({
        [styles.text]: true,
        [styles.textHidden]: props.spinner,
        [styles.reverse]: props.reverse
      })}>{props.icon}{props.children}</span>
      <div className={classNames({
        [styles.spinner]: true,
        [styles.spinnerVisible]: props.spinner,
      })}>
        <Spinner size={22} color="#fff" secondaryColor="rgba(255,255,255,0.4)" />
      </div>
    </button>
  )
}

