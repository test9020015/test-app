import styles from './index.module.scss'
import { ReactElement } from 'react'
import DOMPurify from 'isomorphic-dompurify'

interface Props {
  html?: string
  children?: ReactElement | ReactElement[]
}

export default function HtmlText(props: Props) {

  return (
    <div
      className={styles.root}
      dangerouslySetInnerHTML={props.html ? { __html: DOMPurify.sanitize(props.html) } : undefined}
    >
      {props.children}
    </div>
  )
}
HtmlText.defaultProps = {

}
