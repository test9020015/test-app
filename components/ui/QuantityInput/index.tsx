import styles from './index.module.scss'
import { ChangeEvent } from 'react'

interface Props {
  onChange: (value: string) => void
  value: string
}

export default function QuantityInput(props: Props) {

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const newValue = e.target.value
    props.onChange(newValue)
  }

  return (
    <input className={styles.root} value={props.value} pattern={'[0-9]*'} onChange={handleChange} onInput=
      {(e: ChangeEvent<HTMLInputElement>) => { e.target.value = e.target.value.replace(/[^0-9]/g, '') }} />
  )
}
