import styles from './index.module.scss'
import { Form, FormikProvider, useFormik } from 'formik'
import Validator from '@/utils/validator'
import Button from '@/components/ui/Button'
import { useEffect, useState } from 'react'
import { IOrder } from '@/data/interfaces/IOrder'
import { useOrderContext } from '@/context/order'
import PhoneField from '../filelds/PhoneField'
import OrderRepository from '@/data/repositories/OrderRepository'
import Formatter from '@/utils/formatter'
import { useAppContext } from '@/context/state'
import { ModalType } from '@/types/enums'

interface Props {
  
}

export default function OrderForm(props: Props) {

  const [loading, setLoading] = useState(false)
  const orderContext = useOrderContext()
  const appContext = useAppContext()

  const handleSubmit = async (data: IOrder) => {
    setLoading(true)
    const formattedCart = data.cart.map(item => {
      const { title, price, ...rest } = item
      return rest
    })
    const formattedData = { phone: Formatter.removeNonNumeric(data.phone), cart: formattedCart }
    try {
      const res = await OrderRepository.send(formattedData)
      if (res.success) {
        appContext.showModal(ModalType.OrderSended)
      }
    } catch (err) {
      console.error(err)
    }
    setLoading(false)
  }

  useEffect(() => {
    // Update formik values when order context changes
    formik.setValues((prevValues) => ({
      ...prevValues,
      phone: orderContext.order.phone,
      cart: orderContext.order.cart,
    }))
  }, [orderContext.order.cart, orderContext.order.phone])

  const initialValues: IOrder = {
    phone: orderContext.order.phone,
    cart: orderContext.order.cart
  }

  const formik = useFormik({
    initialValues,
    onSubmit: handleSubmit
  })

  console.log(formik.values)

  const findProductNameById = (id: number) => {
    const product = orderContext.order.cart.find(item => item.id === id)
    return product?.title
  }

  const findProductPriceById = (id: number) => {
    const product = orderContext.order.cart.find(item => item.id === id)
    return product?.price
  }

  return (
    <FormikProvider value={formik}>
      <Form className={styles.form}>
        <div className={styles.left}>
          <div className={styles.title}>
            Добавленные товары
          </div>
          <div className={styles.table}>
            {formik.values.cart.map((i, index) =>
              <div className={styles.item} key={i.id}>
                <div className={styles.name}>
                  {findProductNameById(i.id)}
                </div>
                <div className={styles.space} />
                <div className={styles.quantity}>
                  x{i.quantity}
                </div>
                <div className={styles.price}>
                  {findProductPriceById(i.id) as number * i.quantity}
                </div>
              </div>
            )}
          </div>
          <PhoneField name='phone' validate={Validator.combine([Validator.phone, Validator.required])} />
        </div>
        <div className={styles.right}>
          <Button
            spinner={loading}
            type='submit'
            className={styles.btn}>заказать</Button>
        </div>
      </Form>
    </FormikProvider>
  )
}
