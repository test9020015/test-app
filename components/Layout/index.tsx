import { ReactElement } from 'react'
import styles from './index.module.scss'
import Header from './Header'

interface Props {
  children?: ReactElement | ReactElement[]
}

export default function Layout({ children }: Props) {

  return (
    <div className={styles.root}>
      <div className={styles.container}>
        <Header />
      </div>
      <div className={styles.container_inner}>
        {children}
      </div>
    </div>
  )
}
