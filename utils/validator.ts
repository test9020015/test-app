import { FieldValidator } from 'formik/dist/types'
import Formatter from './formatter'

export default class Validator {
  static emailRe = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,12}$/i

  static combine(validators: FieldValidator[]): FieldValidator {
    return (value: any) => {
      for (let i = 0; i < validators.length; i++) {
        const err = validators[i](value)
        if (err) {
          return err
        }
      }
      return undefined
    }
  }

  static required(value: string | number): string | undefined {
    return value || typeof value === 'number' ? undefined : 'form_field_validation_required'
  }

  static phone(value: string): string | undefined{
    return Formatter.cleanPhone(value ?? '')?.length >= 12 ? undefined : 'Неверный формат'
  }

}
