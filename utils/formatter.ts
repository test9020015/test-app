

export default class Formatter {


  static cleanPhone(phone: string) {
    if (phone) {
      let phoneCleaned = `${phone}`.replace(/[^\+0-9]/g, '')
      if (!phoneCleaned.startsWith('+')) {
        phoneCleaned = '+' + phoneCleaned
      }
      return phoneCleaned
    }
    return phone
  }

  static removeNonNumeric(str: string) {
    // Use a regular expression to match only numeric characters
    // and replace all non-numeric characters with an empty string
    return str.replace(/\D/g, '')
  }

}
