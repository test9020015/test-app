import { ICartItem } from '@/data/interfaces/ICartItem'
import { IOrder } from '@/data/interfaces/IOrder'
import React, { createContext, useContext, useEffect, useState } from 'react'

interface IState {
  order: IOrder,
  updateItemQuantity: (itemId: number, newQuantity: number) => void
  addNewItemToCart: (newItem: ICartItem) => void
  removeItemFromCart: (itemId: number) => void
  changePhoneNumber: (value: string) => void
}

const defaultValue: IState = {
  order: { phone: '+7 (___) ___-__-__', cart: [] },
  updateItemQuantity: (item, newQuantity) => null,
  addNewItemToCart: (newItem) => null,
  removeItemFromCart: (itemId) => null,
  changePhoneNumber: (value) => null
}

const OrderContext = createContext<IState>(defaultValue)

interface Props {
  children: React.ReactNode
}

export function OrderWrapper(props: Props) {

  const [order, setOrder] = useState<IOrder>(defaultValue.order)

  useEffect(() => {
    // Load data from local storage on component mount
    const storedData = localStorage.getItem('order')
    if (storedData) {
      setOrder(JSON.parse(storedData))
    }
    else {
      localStorage.setItem('order', JSON.stringify(order))
    }
  }, [])

  const removeItemFromCart = (itemId: number) => {
    const updatedCart = order.cart.filter(item => item.id !== itemId)
    setOrder(prevOrder => ({ ...prevOrder, cart: updatedCart }))
    localStorage.setItem('order', JSON.stringify({ ...order, cart: updatedCart }))
  }

  const addNewItemToCart = (newItem: ICartItem) => {
    const updatedCart = [...order.cart, newItem]
    setOrder(prevOrder => ({ ...prevOrder, cart: updatedCart }))
    localStorage.setItem('order', JSON.stringify({ ...order, cart: updatedCart }))
  }

  const updateItemQuantity = (itemId: number, newQuantity: number) => {
    const updatedCart = order.cart.map(item =>
      item.id === itemId ? { ...item, quantity: newQuantity } : item
    )
    setOrder(prevOrder => ({ ...prevOrder, cart: updatedCart }))
    localStorage.setItem('order', JSON.stringify({ ...order, cart: updatedCart }))
  }

  const changePhoneNumber = (value: string) => {
    setOrder(prevOrder => ({ ...prevOrder, phone: value }))
    localStorage.setItem('order', JSON.stringify({ ...order, phone: value }))
  }

  const value: IState = {
    ...defaultValue,
    order: order,
    addNewItemToCart: addNewItemToCart,
    updateItemQuantity: updateItemQuantity,
    removeItemFromCart: removeItemFromCart,
    changePhoneNumber: changePhoneNumber,
  }

  return <OrderContext.Provider value={value}>{props.children}</OrderContext.Provider>
}

export function useOrderContext() {
  return useContext(OrderContext)
}
