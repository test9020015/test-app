export interface IRequestStatus {
    success: number
    error?: string
}
