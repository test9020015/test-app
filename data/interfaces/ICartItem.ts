export interface ICartItem {
    id: number
    quantity: number
    title?: string
    price?: number
}
