import { IProduct } from "./IProduct"

export interface IProductsRequest {
  page: number
  amount: number
  total: number
  products: IProduct[]
}
