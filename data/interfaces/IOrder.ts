import { ICartItem } from "./ICartItem"

export interface IOrder {
  phone: string
  cart: ICartItem[]
}
