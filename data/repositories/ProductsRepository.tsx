import request from 'utils/request'
import { IProductsRequest } from '../interfaces/IProductsRequest'

export default class ProductsRepository {

  static async fetch(page: number, page_size: number = 20): Promise<IProductsRequest> {
    const res = await request<IProductsRequest>({
      method: 'get',
      url: `/products?page=${page}&page_size=${page_size}`,
    })
    return res
  }
}
