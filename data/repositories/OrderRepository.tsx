import request from 'utils/request'
import { IOrder } from '../interfaces/IOrder'
import { IRequestStatus } from '../interfaces/IRequestStatus'

export default class OrderRepository {

  static async send(data: IOrder): Promise<IRequestStatus> {
    const res = await request<IRequestStatus>({
      method: 'post',
      url: '/order',
      data
    })
    return res
  }
}
