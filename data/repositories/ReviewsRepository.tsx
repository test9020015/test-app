import request from 'utils/request'
import { IReview } from '@/data/interfaces/IReview'

export default class ReviewsRepository {

  static async fetch(): Promise<IReview[]> {
    const res = await request<IReview[]>({
      method: 'get',
      url: '/reviews',
    })
    return res
  }
}
