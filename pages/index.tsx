import styles from './index.module.scss'
import Layout from '@/components/Layout'
import OrderForm from '@/components/form'
import Products from '@/components/products'
import Reviews from '@/components/reviews'
import { useAppContext } from '@/context/state'
import { IProduct } from '@/data/interfaces/IProduct'
import { IReview } from '@/data/interfaces/IReview'
import ProductsRepository from '@/data/repositories/ProductsRepository'
import ReviewsRepository from '@/data/repositories/ReviewsRepository'

interface Props {
  reviews: IReview[]
  products: IProduct[]
  total: number
  page: number
}

export default function IndexPage(props: Props) {

  const appContext = useAppContext()

  return (
    <Layout>
      <Reviews reviews={props.reviews} />
      <div className={styles.form_wrapper}>
        <OrderForm />
      </div>
      <Products totalItems={props.total} products={props.products} whichPage={props.page} />
    </Layout>
  )
}

export async function getServerSideProps() {
  // Fetch data from external API
  const reviewsRes = await ReviewsRepository.fetch()
  const productsRes = await ProductsRepository.fetch(1, 6)
  const reviews = reviewsRes
  const products = productsRes.products
  const total = productsRes.total
  const page = productsRes.page

  // Pass data to the page via props
  return { props: { reviews, products, total, page } }
}