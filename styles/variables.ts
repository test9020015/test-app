export const breakpoints = {
  VLDesktopWidth: 1920,
  LDesktopWidth: 1440,
  DesktopWidth: 1280,
  MdDesktopWidth: 1200,
  SmDesktopWidth: 1024,
  VSmDesktopWidth: 992,
  TabletWidth: 768,
  PhoneWidth: 480,
  LPhoneWidth: 428,
  MPhoneWidth: 375,
  SmPhoneWidth: 320,


  xsMax: 768,
  mdMin: 769,
  mdMedium: 1200,
  mdMax: 1600,
  lgMin: 1601,
  lgMedium: 1700,
  lgMax: 2500,
  xlMin: 2501,
  xlMaxFake: 8000,
}

export const colors = {
  white: '#F0F0F0',
  black: '#000000',
  header: '#777777',
  main: '#222222',
  item: '#D9D9D9',
  button: '#222222'
}
