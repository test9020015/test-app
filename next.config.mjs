/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
            {
                protocol: 'https',
                hostname: 'encrypted-tbn**.gstatic.com',
                port: '',
            },
        ]
    },
};

export default nextConfig;
